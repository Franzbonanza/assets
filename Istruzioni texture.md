TODO: temp documentation, to translate in English eventually

### Creazione texture
Premessa: le texture sono sempre 2048x2048
1. creare gruppi di vertici seguendo la nomenclatura Col1, 2 ecc. Questo faciliterà la selezione
2. Creare texture b/n aiutandosi con gruppi di vertici, seguendo medesima nomenclatura. Bianco = area da colorare, nero = area da ignorare.
3. Controllare che non siano state selezionate facce extra perché adiacenti, ritoccandole
3. Usare nodi "MixRGB" per unire le texture dandoci colore, dove ogni nodo prende:
    * come `fac` la texture
    * come `Color1` il mix precedente (o il colore nero se è il primo)
    * come `Color2` il colore da assegnare a quella texture
4. Eventuali ritocchi
5. Cuocere il risultato in una texture a parte denominata `diffuse`, assicurandosi che `Principled BSDF` non abbia valori alterati né altre texture connesse se non quelle che servono per la cottura. Questo è necessario perché sennò il formato .glb non legge la texture (forse verrà sistemato in futuro?)
